/*
 * Graphics menu system, u8g2 version, uses inverted bars for selection
 * Uses more memory (about 86%!)
 * 
 * Combination of 2 examples:
 * https://github.com/neu-rah/ArduinoMenu/blob/master/examples/U8G2/U8G2/U8G2.ino
 * https://github.com/neu-rah/ArduinoMenu/blob/master/examples/btnNav/btnNav/btnNav.ino
 * 
 * Required libraries:
 * ArduinoMenu https://github.com/neu-rah/ArduinoMenu/ (install via Tools > Manage Libraries)
*/

#include <Arduino.h>
#include <Wire.h>
#include <menu.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/altKeyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/u8g2Out.h>

using namespace Menu;

#define LEDPIN LED_BUILTIN
#define SOFT_DEBOUNCE_MS 100
#define MAX_DEPTH 2

// pins for buttons:
#define BTN_SEL 6  // Select button
#define BTN_UP 4 // Up
#define BTN_DOWN 5 // Down

#define fontName u8g2_font_7x13_mf
#define fontX 7
#define fontY 16
#define offsetX 0
#define offsetY 3
#define U8_Width 128
#define U8_Height 64
#define USE_HWI2C

U8G2_SSD1306_128X64_NONAME_1_HW_I2C display(U8G2_R0, U8X8_PIN_NONE, 4, 5);

//U8X8_SSD1306_128X64_NONAME_HW_I2C display(U8X8_PIN_NONE);

// define menu colors --------------------------------------------------------
//each color is in the format:
//  {{disabled normal,disabled selected},{enabled normal,enabled selected, enabled editing}}
// this is a monochromatic color table
const colorDef<uint8_t> colors[6] MEMMODE={
  {{0,0},{0,1,1}},//bgColor
  {{1,1},{1,0,0}},//fgColor
  {{1,1},{1,0,0}},//valColor
  {{1,1},{1,0,0}},//unitColor
  {{0,1},{0,0,1}},//cursorColor
  {{1,1},{1,0,0}},//titleColor
};

char* constMEM hexDigit MEMMODE="0123456789ABCDEF";
char* constMEM hexNr[] MEMMODE={"0","x",hexDigit,hexDigit};
char buf1[]="0x11";//<-- menu will edit this text

int chooseTest=-1;
int ledCtrl=LOW;

result myLedOn() {
  ledCtrl=HIGH;
  return proceed;
}
result myLedOff() {
  ledCtrl=LOW;
  return proceed;
}

CHOOSE(chooseTest,chooseMenu,"Choose",doNothing,noEvent,noStyle
  ,VALUE("First",1,doNothing,noEvent)
  ,VALUE("Second",2,doNothing,noEvent)
  ,VALUE("Third",3,doNothing,noEvent)
  ,VALUE("Last",-1,doNothing,noEvent)
);

TOGGLE(ledCtrl,setLed,"Led: ",doNothing,noEvent,wrapStyle//,doExit,enterEvent,noStyle
  ,VALUE("On",HIGH,doNothing,noEvent)
  ,VALUE("Off",LOW,doNothing,noEvent)
);

MENU(mainMenu,"Main menu",doNothing,noEvent,wrapStyle
  ,OP("Option 1",doNothing,noEvent)
  ,EDIT("Hex",buf1,hexNr,doNothing,noEvent,noStyle)
  ,SUBMENU(chooseMenu)
  ,SUBMENU(setLed)
  ,EXIT("<Back")
);

MENU_OUTPUTS(out,MAX_DEPTH
  ,SERIAL_OUT(Serial)
  ,U8G2_OUT(display,colors,fontX,fontY,offsetX,offsetY,{0,0,U8_Width/fontX,U8_Height/fontY}) /* x_offset, y_offset, x_width, y_width*/
);

// define button list and functions:
keyMap btn_map[]={
 {BTN_SEL, defaultNavCodes[enterCmd].ch, INPUT_PULLUP},
 {BTN_UP, defaultNavCodes[upCmd].ch, INPUT_PULLUP},
 {BTN_DOWN, defaultNavCodes[downCmd].ch, INPUT_PULLUP}
};
keyIn<3> btns(btn_map); // 3 buttons

serialIn serial(Serial);
menuIn* inputsList[]={&serial, &btns};
chainStream<2> in(inputsList);//2 is the number of inputs

NAVROOT(nav,mainMenu,MAX_DEPTH,in,out);

void setup() {
  pinMode(LEDPIN,OUTPUT);
  Serial.begin(9600);
  while(!Serial);
  btns.begin();
  Serial.println("Menu test");
  display.begin();
  display.setFont(fontName);
  display.drawStr(0,0,"Menu test");
  delay(1000);
}

void loop() {
  nav.doInput();
  if (nav.changed(0)) {//only draw if menu changed for gfx device
    //change checking leaves more time for other tasks
    display.firstPage();
    do nav.doOutput(); while(display.nextPage());
  }
  digitalWrite(LEDPIN, ledCtrl);
  delay(100);
}
