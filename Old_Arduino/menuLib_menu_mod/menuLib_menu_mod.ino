/*
 * Text-based menu system based on ArduinoMenu library.
 * 
 * Combination of 2 examples:
 * https://github.com/neu-rah/ArduinoMenu/blob/master/examples/U8x8/U8x8/U8x8.ino
 * https://github.com/neu-rah/ArduinoMenu/blob/master/examples/btnNav/btnNav/btnNav.ino
 * 
 * Required libraries:
 * ArduinoMenu https://github.com/neu-rah/ArduinoMenu/ (install via Tools > Manage Libraries)
*/

#include <Arduino.h>
#include <Wire.h>
#include <menu.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/altKeyIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/U8x8Out.h>

using namespace Menu;

#define LEDPIN 27
#define SOFT_DEBOUNCE_MS 100
#define MAX_DEPTH 2

// pins for buttons:
#define BTN_SEL 17  // Select button
#define BTN_UP 14 // Up
#define BTN_DOWN 13 // Down

//U8X8_SSD1306_128X64_NONAME_HW_I2C display(U8X8_PIN_NONE);
U8X8_SSD1306_128X32_UNIVISION_SW_I2C display(/* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);

char* constMEM hexDigit MEMMODE="0123456789ABCDEF";
char* constMEM hexNr[] MEMMODE={"0","x",hexDigit,hexDigit};
char buf1[]="0x11";//<-- menu will edit this text

int chooseTest=-1;
int ledCtrl=LOW;

result myLedOn() {
  ledCtrl=HIGH;
  return proceed;
}
result myLedOff() {
  ledCtrl=LOW;
  return proceed;
}

CHOOSE(chooseTest,chooseMenu,"Choose",doNothing,noEvent,noStyle
  ,VALUE("wifi",1,doNothing,noEvent)
  ,VALUE("kbt",2,doNothing,noEvent)
  ,VALUE("Last",-1,doNothing,noEvent)
);

TOGGLE(ledCtrl,setLed,"Led: ",doNothing,noEvent,wrapStyle//,doExit,enterEvent,noStyle
  ,VALUE("On",HIGH,doNothing,noEvent)
  ,VALUE("Off",LOW,doNothing,noEvent)
);

MENU(mainMenu,"Main menu",doNothing,noEvent,wrapStyle
  //,OP("wifi",doNothing,noEvent)
  //,EDIT("Hex",buf1,hexNr,doNothing,noEvent,noStyle)
  ,SUBMENU(chooseMenu)
  ,SUBMENU(setLed)
  ,EXIT("<Back")
);

MENU_OUTPUTS(out,MAX_DEPTH
  ,SERIAL_OUT(Serial)
  ,U8X8_OUT(display,{0,0,16,8}) /* x_offset, y_offset, x_width, y_width*/
);

// define button list and functions:
keyMap btn_map[]={
 {BTN_SEL, defaultNavCodes[enterCmd].ch, INPUT_PULLUP},
 {BTN_UP, defaultNavCodes[upCmd].ch, INPUT_PULLUP},
 {BTN_DOWN, defaultNavCodes[downCmd].ch, INPUT_PULLUP}
};
keyIn<3> btns(btn_map); // 3 buttons

serialIn serial(Serial);
menuIn* inputsList[]={&serial, &btns};
chainStream<2> in(inputsList);//2 is the number of inputs

NAVROOT(nav,mainMenu,MAX_DEPTH,in,out);

void setup() {
  pinMode(LEDPIN,OUTPUT);
  Serial.begin(115200);
  while(!Serial);
  btns.begin();
  Serial.println("Menu test");
  display.begin();
  display.setFont(u8x8_font_chroma48medium8_r);
  //display.drawString(0,0,"Menu test");
  delay(1000);
}

void loop() {
  nav.poll();
  digitalWrite(LEDPIN, ledCtrl);
  delay(100);
}
