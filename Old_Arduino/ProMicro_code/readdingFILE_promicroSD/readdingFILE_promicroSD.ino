// program exersise string compehenton
# include <SPI.h>
#include <SD.h>

const int CS =8;  // chip select pin
File fileinst;

void setup()
{
  Serial.begin(9600);
  while(!Serial)
  {
    ; // empty loop  wait for serial port to connect
  }
  
  SD.begin(CS);// initialise sd card
  
  fileinst = SD.open("user.txt"); // open the file

  if(fileinst)
  {
    Serial.println("user.txt contains");
    
    while (fileinst.available())  // read from the file and print
    {
      Serial.write(fileinst.read());
    }
    
    fileinst.close();
  }
  else
  {
    Serial.println("error opening user.txt");
  }
}

void loop()
{
}
