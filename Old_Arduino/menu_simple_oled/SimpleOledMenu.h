#ifndef SIMPLEOLEDMENU_H_
#define SIMPLEOLEDMENU_H_

#include "MenuClass.h"
#include <U8g2lib.h>

// pins for up/down/select buttons
#define UP_BTN 4
#define DOWN_BTN 5
#define SELECT_BTN 6

class SimpleOledMenu : public MenuClass {
public:
  U8X8_SSD1306_128X64_NONAME_HW_I2C * display;

  // constructor:
  SimpleOledMenu();

  // methods:
  void init();
  int updateSelection();
  boolean selectionMade();
  void displayMenu();
};

#endif
