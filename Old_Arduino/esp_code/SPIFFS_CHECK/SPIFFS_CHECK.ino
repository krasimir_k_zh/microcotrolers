#include <Vector.h>
#include "FS.h"
#include "SPIFFS.h"
File myFile;
 
#define FORMAT_SPIFFS_IF_FAILED false //sue first time after that cange it to false
// function definiton reading files
void listFiles(File dir, int numTabs = 1);

void setup()
{
    Serial.begin(112500);
 
    delay(500);
 
    Serial.println(F("Inizializing FS..."));
    if (SPIFFS.begin())
    {
        Serial.println(F("SPIFFS mounted correctly."));
    }
    else
    {
        Serial.println(F("!An error occurred during SPIFFS mounting"));
        if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED))
        {
        Serial.println("FORMATING SPIFFS");
        return;
        }
    }

    // Get all information of SPIFFS
 
    unsigned int totalBytes = SPIFFS.totalBytes();
    unsigned int usedBytes = SPIFFS.usedBytes();
 
    Serial.println("===== File system info =====");
 
    Serial.print("Total space:      ");
    Serial.print(totalBytes);
    Serial.println("byte");
 
    Serial.print("Total space used: ");
    Serial.print(usedBytes);
    Serial.println("byte");
 
    Serial.println();
 
    // Open dir folder
    File dir = SPIFFS.open("/");
    // List file at root
    listFiles(dir);

    myFile = SPIFFS.open("/users", FILE_READ);
    Serial.println("File content: ");
    while(myFile.available())
    {// napravi for lup za vseki red
    Serial.write(myFile.read());
    }
} 

void listFiles(File dir, int numTabs)
{
  while (true)
  { 
    File entry =  dir.openNextFile();
    if (! entry)
    { // no more files in the folder
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++)
    {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory())
    {
      Serial.println("/");
      listFiles(entry, numTabs + 1);
    }
    else
    {
      // display zise for file, nothing for directory
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}






void loop() {
 
} 
