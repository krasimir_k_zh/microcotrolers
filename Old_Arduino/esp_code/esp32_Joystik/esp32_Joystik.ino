const int pin_up = 13;
const int pin_dwn = 14;
const int pin_lft = 15;
const int pin_rht = 16;
const int pin_mid = 17;
const int pin_set = 18;
const int pin_rst = 19;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(pin_up, INPUT_PULLUP);
  pinMode(pin_dwn, INPUT_PULLUP);
  pinMode(pin_lft, INPUT_PULLUP);
  pinMode(pin_rht, INPUT_PULLUP);
  pinMode(pin_mid, INPUT_PULLUP);
  pinMode(pin_set, INPUT_PULLUP);
  pinMode(pin_rst, INPUT_PULLUP);
  
  Serial.println("BEGIN");

}

void loop() {
  // put your main code here, to run repeatedly:
    int UP = digitalRead(pin_up);
    int DWN = digitalRead(pin_dwn);
    int LFT = digitalRead(pin_lft);
    int RHT = digitalRead(pin_rht);
    int MID = digitalRead(pin_mid);
    int SET = digitalRead(pin_set);
    int RST = digitalRead(pin_rst);
    
    if (UP == LOW) Serial.println("UP presed");
    if (DWN == LOW) Serial.println("DWN presed");
    if (LFT == LOW) Serial.println("LFT presed");
    if (RHT == LOW) Serial.println("RHT presed");
    if (MID == LOW) Serial.println("MID presed");
    if (SET == LOW) Serial.println("SET presed");
    if (RST == LOW) Serial.println("RST presed");
    delay(150);
}
