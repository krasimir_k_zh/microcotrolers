const int pin_button = 4;  // using pin 4 
int button_state = 0;      // variable to save the state of the button
int status_flag = 0;       // on of flag

void setup() {
  
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(pin_button, INPUT_PULLUP);
  Serial.println("BEGIN");

}

void loop() {
  // put your main code here, to run repeatedly:
  button_state = digitalRead(pin_button);
  //Serial.println(status_flag);
  if(button_state == LOW){
    delay (250); 
    if(status_flag == 0){
      status_flag =1;
      }
  else{
    status_flag = 0;
    }
  }
   
  Serial.println(status_flag);
}
