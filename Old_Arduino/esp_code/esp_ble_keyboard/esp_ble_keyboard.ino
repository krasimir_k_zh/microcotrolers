/**
 * This example turns the ESP32 into a Bluetooth LE keyboard that writes the words, presses Enter, presses a media key and then Ctrl+Alt+Delete
 */
#include <BleKeyboard.h>

const int pin_button = 4; // using pin 4 
int button_state = 0;      // variable to save the state of the button
int status_flag = 0;       // on of flag

//BleKeyboard bleKeyboard;
BleKeyboard bleKeyboard("Krasula KB", "Krasimir", 100);

void setup() {
  Serial.begin(115200);
  pinMode(pin_button, INPUT_PULLUP);   // seting pin 4 as a input
  Serial.println("Starting BLE work!");
  bleKeyboard.begin();
}

void loop() {
  button_state = digitalRead(pin_button);  // reading te pin 
  // creating togle variable 
  if(button_state == LOW){
    delay (250); 
    if(status_flag == 0){
      status_flag = 1;
      }
  else{
     status_flag = 0;
      }
  }
  Serial.println(status_flag);
  
  if(bleKeyboard.isConnected() && status_flag == 1) {      // keystoke will be emited oly if  
    Serial.println("Begin'...");                           // blutoot is conected and status flag  is togled to  one
    bleKeyboard.print("Begin");                            // 
    bleKeyboard.print("\n");

    //delay(1000);

    //Serial.println("Sending Enter key...");
    //bleKeyboard.write(KEY_RETURN);

    //delay(1000);

    Serial.println("Hello world...");
    bleKeyboard.print("H");
    bleKeyboard.print("e");
    bleKeyboard.print("l");
    bleKeyboard.print("o");
    bleKeyboard.print(" ");
    bleKeyboard.print("World");
    bleKeyboard.print("\n");

    delay(1000);

    
/*
    Serial.println("Sending Ctrl+Alt+Delete...");
    bleKeyboard.press(KEY_LEFT_CTRL);
    bleKeyboard.press(KEY_LEFT_ALT);
    bleKeyboard.press(KEY_DELETE);
    delay(100);
    bleKeyboard.releaseAll();*/
  }

  //delay(5000);

}
