
#include "SimpleOledMenu.h"

SimpleOledMenu::SimpleOledMenu() {
  //display = new U8X8_SSD1306_128X64_NONAME_HW_I2C(U8X8_PIN_NONE);
  display = new U8X8_SSD1306_128X32_UNIVISION_SW_I2C(/* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);
}

void SimpleOledMenu::init() {
  pinMode(UP_BTN, INPUT_PULLUP);
  pinMode(DOWN_BTN, INPUT_PULLUP);
  pinMode(SELECT_BTN, INPUT_PULLUP);
  display->begin();
  display->setPowerSave(0);
  display->setFont(u8x8_font_pxplusibmcgathin_f);
}

int SimpleOledMenu::updateSelection() {
  // This is a simplified example
  // Edge detection and debouncing are exercises left to the user
  int i = 0;
  if (digitalRead(UP_BTN) == LOW) i = 1;
  if (digitalRead(DOWN_BTN) == LOW) i = -1;
  if (i!=0) {
    display->setCursor(0, currentItemIndex);
    display->print(' ');
    currentItemIndex += i;
    //  Avoid negative numbers with the modulo
    //  to roll the menu over properly
    while (currentItemIndex < 0) {
      currentItemIndex += currentMenu->getSize();
    }
    currentItemIndex %= currentMenu->getSize();
    display->setCursor(0, currentItemIndex);
    display->print('>');
  }
  return i;
}

boolean SimpleOledMenu::selectionMade() {
  // This is a simplified example
  // Edge detection and debouncing are exercises left to the user
  return !digitalRead(SELECT_BTN);
}

void SimpleOledMenu::displayMenu() {
  display->clear();
  char buf[16]; // text buffer
  for (int i = 0; i < currentMenu->getSize(); i++) { // for each menu item
    display->setCursor(0, i);
    if (i == currentItemIndex) {
      display->print('>');
    }
    else {
      display->print(' ');
    }
    // use getText method to pull text out into a buffer you can print
    getText(buf, i); // get menu-text of item i
    display->print(buf);
  }
}
