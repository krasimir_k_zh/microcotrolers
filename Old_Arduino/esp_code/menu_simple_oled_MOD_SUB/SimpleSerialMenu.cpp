
#include "SimpleSerialMenu.h"

SimpleSerialMenu::SimpleSerialMenu() {
  display = new U8X8_SSD1306_128X64_NONAME_HW_I2C(U8X8_PIN_NONE);
}

void SimpleSerialMenu::init() {
  pinMode(upButtonPin, INPUT_PULLUP);
  pinMode(downButtonPin, INPUT_PULLUP);
  pinMode(selectButtonPin, INPUT_PULLUP);
  display->begin();
  display->setPowerSave(0);
  display->setFont(u8x8_font_pxplusibmcgathin_f);
}

int SimpleSerialMenu::updateSelection() {
  // This is a simplified example
  // Edge detection and debouncing are exercises left to the user
  int i = 0;
  if (digitalRead(upButtonPin) == LOW) {
    i = 1;
  }
  if (digitalRead(downButtonPin) == LOW) {
    i = -1;
  }
  if (i!=0) {
    display->setCursor(0, currentItemIndex);
    display->print(' ');
    currentItemIndex += i;
    //  Avoid negative numbers with the modulo
    //  to roll the menu over properly
    while (currentItemIndex < 0) {
      currentItemIndex += currentMenu->getSize();
    }
    currentItemIndex %= currentMenu->getSize();
    display->setCursor(0, currentItemIndex);
    display->print('>');
  }
  return i;
}

boolean SimpleSerialMenu::selectionMade() {
  // This is a simplified example
  // Edge detection and debouncing are exercises left to the user
  return !digitalRead(selectButtonPin);
}

void SimpleSerialMenu::displayMenu() {
  display->clear();
  for (int i = 0; i < currentMenu->getSize(); i++) {
    char buf[16]; // text buffer
    display->setCursor(0, i);
    if (i == currentItemIndex) {
      display->print('>');
    }
    else {
      display->print(' ');
    }
    // use getText method to pull text out into a buffer you can print
    getText(buf, i);
    display->print(buf);
  }
}
