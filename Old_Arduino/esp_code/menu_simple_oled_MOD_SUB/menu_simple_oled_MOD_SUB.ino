/**
 * This menu and some alternatives are described in this Blog post:
 * https://home.et.utwente.nl/slootenvanf/2019/11/15/using-oled-display-with-arduino/
 * 
 * Slightly improved version of:
 * https://forum.arduino.cc/index.php?topic=353045.0
 * and now works on a OLED display (requires library U8g2lib)
 * 
 * Button pins for the up/down/select buttons are defined in SimpleOledMenu.h
 * This sketch does not use debouncing or a library for that, to reduce its memory usage.
 * Instead a delay is added to the loop()
 * This delay might need adjustment depending on the amount of code you add to other parts of the sketch.
 * 
 * To reduce use of RAM memory, the menu is stored in program memory (PROGMEM).
 * Learn more:
 * https://playground.arduino.cc/Learning/Memory/
 * https://www.arduino.cc/reference/en/language/variables/utilities/progmem/
 */

#include "SimpleOledMenu.h"

// prototypes of functions used in the menu
boolean fun1();
boolean fun2();
boolean fun3();
boolean fun4();
boolean gotoMenuOne();
boolean gotoMenuTwo();

/*****************************
 ******************************
 * Setup the menu as an array of MenuItem
 * Create a MenuList and an instance of your 
 * menu class
 ******************************
 *****************************/

MenuItem PROGMEM firstList[3] = { 
  { "WIFI", fun1 },
  { "BLT", fun2 },
  { "USER", gotoMenuTwo }
};

MenuItem PROGMEM secondList[3] = {
  { "Back" , gotoMenuOne },
  { "Item_three", fun3 },
  { "Item_four" , fun4 }
};

MenuList list1(firstList, 3);
MenuList list2(secondList, 3);

SimpleOledMenu menu;


void setup(){
  Serial.begin(115200);
  menu.init();
  menu.setCurrentMenu(&list1);
  Serial.println(F("Running Menu Test:"));
}

void loop(){
  menu.doMenu();

  //  Whatever other code you have.  
  //  Try to keep it non-blocking (avoid delay's) while the menu is displayed  
  delay(250);  // because buttons aren't debounced. 
}


/*****************************
 ******************************
 * Define the functions you want your menu to call
 * They can be blocking or non-blocking
 * They should take no arguments and return a boolean
 * true if the function is finished and doesn't want to run again
 * false if the function is not done and wants to be called again
 ******************************
 *****************************/


boolean fun1() {
  // blocking functions that simply return true will be run once and will complete
  //  before the code goes to the next loop
  Serial.println(F("FUN 1:  running right now and printing this long line"));
  delay(3000);
  return true;
}

boolean fun2() {
  Serial.println(F("FUN 2:  running right now and printing this long line"));
  delay(3000);
  return true;
}

boolean fun3() {
  Serial.println(F("FUN 3:  locked here until button push"));
  delay(100);
  if(digitalRead(SELECT_BTN) == LOW){
    return true;  // signal finished if button pressed
  }
  else {
    return false;  // no button press, keep running
  }
}

boolean fun4() {
  // remember you have to have at least one case return true or you lock things up
  Serial.println(F("FUN 4:  Locked here forever"));
  delay(3000);
  return false;
}

boolean gotoMenuOne() {
  menu.setCurrentMenu(&list1);
  return true;
}

boolean gotoMenuTwo() {
  menu.setCurrentMenu(&list2);
  return true;
}
