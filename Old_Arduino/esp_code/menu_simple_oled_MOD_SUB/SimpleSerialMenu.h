#ifndef SIMPLESERIALMENU_H_
#define SIMPLESERIALMENU_H_

#include "MenuClass.h"
#include <U8g2lib.h>

#define upButtonPin 4
#define downButtonPin 5
#define selectButtonPin 6

class SimpleSerialMenu : public MenuClass {
public:
  U8X8_SSD1306_128X64_NONAME_HW_I2C * display;

  // constructor:
  SimpleSerialMenu();

  // methods:
  void init();
  int updateSelection();
  boolean selectionMade();
  void displayMenu();
};

#endif
