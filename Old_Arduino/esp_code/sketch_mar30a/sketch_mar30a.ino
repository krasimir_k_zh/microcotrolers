#include <WiFi.h>
#include <DNSServer.h>

// captve server setup data
const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
WiFiServer server(80);

#define AP_PASS "Costa123kk"
#define AP_SSID  "Password_manager"

enum { STEP_ON, STEP_OFF, STEP_END  };

void onButton(){
  static uint32_t step = STEP_ON;
  switch(step){
    case STEP_ON://AP Only
      Serial.println("** Starting AP");
      WiFi.disconnect();   //added to start with the wifi off, avoid crashing
      WiFi.mode(WIFI_OFF); //added to start with the wifi off, avoid crashing
      WiFi.mode(WIFI_AP);
      WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
      WiFi.softAP(AP_SSID, AP_PASS);
      dnsServer.start(DNS_PORT, "*", apIP); 
    break;
    case STEP_OFF://All Off
      Serial.println("** Stopping WiFi");
      WiFi.mode(WIFI_OFF);
    break;
    default:
    break;
  }
    if(step == STEP_END){
    step = STEP_ON;
  } else {
    step++;
  }
  //little debounce
  delay(100);
}
