void loop(){
  File file = SPIFFS.open("/counter.txt", "a");
  if(!file){
    // File not found 
    Serial.println("Failed to open counter file");
    return;
  } else {
    counter+=1;
    file.println(counter);
    file.close();
  }
  delay(1000);
}
