void ninthLesson() {
  //READ FROM SERIAL AND PARSE OUT ** READ FROM SERIAL AND PARSE OUT ** READ FROM SERIAL AND PARSE OUT **
  char rawData[100] = "";
  char keyword[] = "Mydata=";
  Serial.println("Send me data like this: Mydata=text,int,long,float");
  while (1) {
    if (Serial.available() > 0) {//new data in
      size_t byteCount = Serial.readBytesUntil('\n', rawData, sizeof(rawData) - 1); //read in data to buffer
      rawData[byteCount] = NULL;//put an end character on the data
      Serial.print("Raw Data = ");
      Serial.println(rawData);

      //now find keyword and parse
      char *keywordPointer = strstr(rawData, keyword);
      if (keywordPointer != NULL) {
        int dataPosition = (keywordPointer - rawData) + strlen(keyword); //should be position after Mydata=

        const char delimiter[] = ",";
        char parsedStrings[4][20];
        int dataCount = 0;

        char *token =  strtok(&rawData[dataPosition], delimiter);//look for first piece of data after keyword until comma
        if (token != NULL && strlen(token) < sizeof(parsedStrings[0])) {
          strncpy(parsedStrings[0], token, sizeof(parsedStrings[0]));
          dataCount++;
        } else {
          Serial.println("token to big");
          strcpy(parsedStrings[0], NULL);
        }

        for (int i = 1; i < 4; i++) {
          token =  strtok(NULL, delimiter);
          if (token != NULL && strlen(token) < sizeof(parsedStrings[i])) {
            strncpy(parsedStrings[i], token, sizeof(parsedStrings[i]));
            dataCount++;
          } else {
            Serial.println("token to big");
            strcpy(parsedStrings[i], NULL);
          }
        }

        Serial.print("Found ");
        Serial.print(dataCount);
        Serial.println(" strings:");
        for (int i = 0; i < 4; i++)
          Serial.println(parsedStrings[i]);


        // convert to variables that we can actually use
        char theText[20];
        int theInt = 0;
        long theLong = 0;
        float theFloat = 0.0;

        if (dataCount == 4) {
          strncpy(theText, parsedStrings[0], sizeof(theText));
          theInt = atoi(parsedStrings[1]);
          theLong = atol(parsedStrings[2]);
          theFloat = atof(parsedStrings[3]);
          Serial.print("The Text = ");
          Serial.println(theText);
          Serial.print("The Int = ");
          Serial.println(theInt);
          Serial.print("The Long = ");
          Serial.println(theLong);
          Serial.print("The Float = ");
          Serial.println(theFloat);
        } else {
          Serial.println("data no good");
        }
      } else {
        Serial.println("sorry no keyword");
      }
    }
  }
}
