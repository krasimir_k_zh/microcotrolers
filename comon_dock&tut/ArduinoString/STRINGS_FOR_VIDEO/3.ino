void thirdLesson() {
  char stringOne[20] = "123";
  char stringTwo[20] = "456";

  strncpy(stringOne, stringTwo, sizeof(stringOne));

  Serial.println(stringOne);// wil be 456

  char someCrazyStringToCopy[] = "what ever you want";
  if (sizeof(someCrazyStringToCopy) <= sizeof(stringOne)) {// a way to check and see if the string will fit
    strncpy(stringOne, someCrazyStringToCopy, sizeof(stringOne));
  } else
    Serial.println("that's too big");

  Serial.println(stringOne);


  strncpy(stringOne, "abc", sizeof(stringOne));
  strncpy(stringTwo, "def", sizeof(stringTwo));

  strncpy(&stringOne[3], stringTwo, sizeof(stringOne));// note that stringTwo is the same as &stringTwo[0]

  Serial.println(stringOne);//will be abcdef


  //move some stuff around, swap "abc" with "def"
  strncpy(stringOne, "abcdef", sizeof(stringOne));
  char abc[20];
  strncpy(abc, stringOne, sizeof(abc));//abc has "abcdef"
  abc[3] = NULL;//makes temp "abc"

  char def[20];
  strncpy(def, &stringOne[3], sizeof(def));//pull just "def" from stringOne

  strncpy(stringOne, def, sizeof(stringOne));//stringOne is "def"
  strncpy(&stringOne[3], abc, sizeof(stringOne));//stringOne is "defabc"

  Serial.println(stringOne);//will be defabc

}
