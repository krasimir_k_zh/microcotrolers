void firstLesson() {

  char oneCharacter = 'a';

  Serial.print("oneCharacter = ");
  Serial.print(oneCharacter);
  Serial.print(" sizeof=");
  Serial.println(sizeof(oneCharacter));//1


  char charString[] = "123456789";
  Serial.print("charString[] = ");
  Serial.print(charString);
  Serial.print(" sizeof=");
  Serial.print(sizeof(charString));//10
  Serial.print(" strlen=");
  Serial.println(strlen(charString));//9

  char charString2[100];
  for (int i = 0; i < sizeof(charString2); i++)
    charString2[i] = random(32, 126); //random ascii

  Serial.print("charString2[100] = ");
  Serial.println(charString2);//garbage
  Serial.println(" ");
  Serial.print("sizeof = ");
  Serial.print(sizeof(charString2));//100
  Serial.print(" strlen = ");
  Serial.println(strlen(charString2));//garbage

  char someString[] = "hello world";
  for (int i = 0; i < strlen(someString); i++)
    charString2[i] = someString[i];

  Serial.println(charString2);// no null at end
  charString2[strlen(someString)] = NULL;
  Serial.println(charString2);//fixed with null

}
