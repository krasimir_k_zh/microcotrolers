void secondLesson() {
  char stringOne[20] = "123";
  char stringTwo[20] = "456";

  Serial.println(stringOne);// wil be 123

  strcat(stringOne, stringTwo);

  Serial.println(stringOne);// wil be 123456

  strcat(stringOne, "789");

  Serial.println(stringOne);// wil be 123456789

  char newString[] = "12345678900";
  if (strlen(newString) + strlen(stringOne) + 1 <= sizeof(stringOne)) {// see if it will all fit in string one
    strcat(stringOne, newString);
    Serial.println(stringOne);// will be a full 19 char 1234567891234567890 +1 for /0
  }
  else {
    Serial.println("error - too big");
  }


//strncat()

}
