void seventhLesson() {

  char someDataFeed[] = "sgklfjgflgwaasdcKevin-56,1000000,4.57687,text,more text";
  char keyword[] = "Kevin";

  char *pointerToFoundData = strstr(someDataFeed, keyword);//go find keyword
  if (pointerToFoundData != NULL) { //found it
    int positionInString = pointerToFoundData - someDataFeed;
    Serial.print("Keyword Starts at ");
    Serial.println(positionInString);

    //now strip out keyword and junk
    char goodData[50];
    strncpy(goodData, &someDataFeed[positionInString + strlen(keyword)], sizeof(goodData));
    Serial.println(goodData);
    Serial.println(" ");


    // PARSE **  PARSE **  PARSE **  PARSE **  PARSE **  PARSE **  PARSE **
    const char delimiter[] = ",";
    char parsedStrings[5][20];
    char *token =  strtok(goodData, delimiter);
    strncpy(parsedStrings[0], token, sizeof(parsedStrings[0]));//first one
    for (int i = 1; i < 5; i++) {
      token =  strtok(NULL, delimiter);
      strncpy(parsedStrings[i], token, sizeof(parsedStrings[i]));
    }

    for (int i = 0; i < 5; i++)
      Serial.println(parsedStrings[i]);//  should have the 5 data strings parsed out

    // CONVERT CONVERT ** CONVERT CONVERT ** CONVERT CONVERT ** CONVERT CONVERT **
    // data should be an int, long, float, text, text
    int firstInteger = atoi(parsedStrings[0]);
    Serial.println(firstInteger);

    long secondLong = atol(parsedStrings[1]);
    Serial.println(secondLong);

    float thirdFloat = atof(parsedStrings[2]);
    Serial.println(thirdFloat);
    
    //now to convert everything back into a string
    char newString[100];
    char floatValue[5];
    dtostrf(thirdFloat, 1, 2, floatValue);//convert float to str
    sprintf(newString, "%s%i,%li,%s,%s,%s", keyword, firstInteger, secondLong, floatValue, parsedStrings[3], parsedStrings[4]);
    Serial.println(newString);

  } else {
    Serial.println("NO KEYWORD");
  }
}
