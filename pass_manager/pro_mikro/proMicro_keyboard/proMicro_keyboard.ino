#include <Keyboard.h>

//variables for lock
int btnpres = 0;
byte lastcount  = 0;
byte lock = 0;
int led = 16;

// setup switches
int sw1 = 9;
int sw2 = 8;
int sw3 = 7;
int sw4 = 6;
int btn = 10;

void setup()
{
  pinMode(sw1, INPUT_PULLUP);
  pinMode(sw2, INPUT_PULLUP);
  pinMode(sw3, INPUT_PULLUP);
  pinMode(sw4, INPUT_PULLUP);
  pinMode(btn, INPUT_PULLUP);
  pinMode(btn, INPUT_PULLUP);
  pinMode(led, OUTPUT);

  Keyboard.begin();       //Init keyboard emulation
}

void loop()
{

   if ( lock == 0 && digitalRead(btn) == LOW)  // check if button was pressed
  {
    btnpres++;                  // increment buttonPresses count
    delay(50);                       // debounce switch
  }
  //if (btnpres > 5) btnpres = 0;         // rollover every fourth press
  if (lock == 0 && lastcount != btnpres)              // only do output if the count has changed
  {
    digitalWrite(led, HIGH);
    delay(150);
    digitalWrite(led, LOW);
  }

  if ( digitalRead(sw4) == 0)
  {
    if (btnpres == 3)
    {
      if (lock == 0)
      {
        digitalWrite (led, HIGH);
        delay(500);
        digitalWrite(led, LOW);
        lock = 1;

      }
    }
  }
//  if (btnpres > 3) btnpres = 0;         // rollover every fourth press

    lastcount = btnpres;    // track last press count



// cchub 0000
  if(lock == 1 && digitalRead(btn)==0 && digitalRead(sw1)==1 && digitalRead(sw2)==1 && digitalRead(sw3)==1 && digitalRead(sw4)==1)
  {
    Keyboard.print("https://www.ccukhub.net");
    Keyboard.write(KEY_RETURN);
    delay(5000);
    Keyboard.print("krasimirzhelev");
    Keyboard.write(KEY_TAB);
    Keyboard.print("Pass0956");
    Keyboard.write(KEY_RETURN);
    delay(5000);
  }



// google 1000
  if (lock == 1 && digitalRead(btn)==0 && digitalRead(sw1)==0 && digitalRead(sw2)==1 && digitalRead(sw3)==1 && digitalRead(sw4)==1)
  {
    Keyboard.print("https://www.google.com");
    Keyboard.write(KEY_RETURN);
    delay(5000);
    Keyboard.print("krasimiredler@gmail.com");
    Keyboard.write(KEY_TAB);
    Keyboard.print("Costa05595773kkz");
    Keyboard.write(KEY_RETURN);
    delay(5000);
 }


// firefox 0100
  if (lock == 1 && digitalRead(btn)==0 && digitalRead(sw1)==1 && digitalRead(sw2)==0 && digitalRead(sw3)==1 && digitalRead(sw4)==1)
  {
    Keyboard.print("https://accounts.firefox.com/signin");
    Keyboard.write(KEY_RETURN);
    delay(5000);
    Keyboard.print("krasimiredler@gmail.com");
    Keyboard.write(KEY_TAB);
    Keyboard.print("Costa123kkz3456");
    Keyboard.write(KEY_RETURN);
    delay(5000);
  }


// gitlab 1100
  if (lock == 1 && digitalRead(btn)==0 && digitalRead(sw1)==0 && digitalRead(sw2)==0 && digitalRead(sw3)==1 && digitalRead(sw4)==1)
  {
    Keyboard.print("https://gitlab.com/users/sign_in");
    Keyboard.write(KEY_RETURN);
    delay(5000);
    Keyboard.print("krasimir_k_zh");
    Keyboard.write(KEY_TAB);
    Keyboard.print("Costa05595773kk");
    Keyboard.write(KEY_RETURN);
    delay(5000);
  }


}
