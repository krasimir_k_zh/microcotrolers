#include <WiFi.h>
#include <DNSServer.h>

// captve server setup data
const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 1, 1);
DNSServer dnsServer;
WiFiServer server(80);

// joystck buttons 
const int pin_up = 13;
const int pin_dwn = 14;
const int pin_lft = 15;
const int pin_rht = 16;
const int pin_mid = 17;
const int pin_set = 18;
const int pin_rst = 19;

// wifi settup data
const char* ssid     = "Password_manager";                                                  // added modificaton name of the wifi  
const char* password = "Costa123kk";                                                        // added modification passowrd 
int status_wifi = 0;

String responseHTML = ""
  "<!DOCTYPE html><html><head><title>CaptivePortal</title></head><body>"
  "<h1>Hello World!</h1><p>This is a captive portal example. All requests will "
  "be redirected here.</p></body></html>";

void setup() { 
  WiFi.disconnect();   //added to start with the wifi off, avoid crashing
  WiFi.mode(WIFI_OFF); //added to start with the wifi off, avoid crashing
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssid, password);                                                              // added 
  //WiFi.softAP("DNSServer CaptivePortal example");                                         // commented of the original code look previous line  

  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);
  
  // pin inicialization
  Serial.begin(115200);
  pinMode(pin_up, INPUT_PULLUP);
  pinMode(pin_dwn, INPUT_PULLUP);
  pinMode(pin_lft, INPUT_PULLUP);
  pinMode(pin_rht, INPUT_PULLUP);
  pinMode(pin_mid, INPUT_PULLUP);
  pinMode(pin_set, INPUT_PULLUP);
  pinMode(pin_rst, INPUT_PULLUP);
  

  server.begin();
}

void loop() {
    // reading pins 
    int UP = digitalRead(pin_up);
    int DWN = digitalRead(pin_dwn);
    int LFT = digitalRead(pin_lft);
    int RHT = digitalRead(pin_rht);
    int MID = digitalRead(pin_mid);
    int SET = digitalRead(pin_set);
    int RST = digitalRead(pin_rst);

  dnsServer.processNextRequest();
  WiFiClient client = server.available();   // listen for incoming clients

  if (client) {
    String currentLine = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        if (c == '\n') {
          if (currentLine.length() == 0) {
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();
            client.print(responseHTML);
            break;
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }
    client.stop();
  }

  // crating togle variable ststus_wifi to control wifi whit one button 
  if (MID == LOW ){
    delay (330);
    if (status_wifi== 0){
      status_wifi =1;
      }
      else{
           status_wifi=0;
          }
    }
    // turning wifi on and of based on ststus_wifi variable 
    if (status_wifi == 0 && MID == 0 ){
      Serial.println (status_wifi);
      Serial.println("** Stopping WiFi");
      WiFi.mode(WIFI_OFF);
       }
     if (MID ==0 && status_wifi ==1){
      Serial.println (status_wifi);
       Serial.println("** Starting  WiFi");
       WiFi.mode(WIFI_AP);
       WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
       WiFi.softAP(ssid, password);
      
     }
    }
