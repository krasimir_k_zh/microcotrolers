"""CircuitPython Essentials Digital In Out example"""
# libray imports
import time
import board
from digitalio import DigitalInOut, Direction, Pull
import displayio
import adafruit_displayio_ssd1306
import terminalio
from adafruit_display_text import label

# display inicialisation
font = terminalio.FONT
displayio.release_displays()
i2c = board.I2C()
display_bus = displayio.I2CDisplay(i2c, device_address=0x3c)
oled = adafruit_displayio_ssd1306.SSD1306(display_bus, width=128, height=32)

# LED setup.
led = DigitalInOut(board.D13)
# For QT Py M0. QT Py M0 does not have a D13 LED, so you can connect an external LED instead.
# led = DigitalInOut(board.SCK)
led.direction = Direction.OUTPUT

# For Gemma M0, Trinket M0, Metro M0 Express, ItsyBitsy M0 Express, Itsy M4 Express, QT Py M0
switch_midle = DigitalInOut(board.D2)
switch_up = DigitalInOut(board.D9)
switch_dwn = DigitalInOut(board.D7)
# switch = DigitalInOut(board.D5)  # For Feather M0 Express, Feather M4 Express
# switch = DigitalInOut(board.D7)  # For Circuit Playground Express
switch_midle.direction = Direction.INPUT
switch_midle.pull = Pull.UP
switch_up.direction = Direction.INPUT
switch_up.pull = Pull.UP
switch_dwn.direction = Direction.INPUT
switch_dwn.pull = Pull.UP

# tuple of all printible character
Alfa = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',\
        'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',\
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',\
        'Y', 'Z', '!', '"', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<',\
        '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~', ' ', '\t')
Current_position = 0
Value_Alfa = 0
while True:
    # We could also do "led.value = not switch.value"!
    # midle boton on pin 2
    if switch_midle.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        print("midle boton presed")
        Current_position = Current_position +1
        print( Current_position)
    # up boton on pin 9
    if switch_up.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        time.sleep(0.1)
        print("upper boton presed")
        Value_Alfa = Value_Alfa + 1
        print(Value_Alfa)
    # down bo on pin 7
    if switch_dwn.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        print("down boton presed")
        Value_Alfa = Value_Alfa - 1
        print(Value_Alfa)
    time.sleep(0.01)  # debounce delay
    # prining on the display

#    time_display = "{:d}:{:02d}:{:02d} {}".format(hour, current.tm_min, current.tm_sec, am_pm)
#    date_display = "{:d}/{:d}/{:d}".format(current.tm_mon, current.tm_mday, current.tm_year)
    Menu_display = "menu            exit"
#    clock = label.Label(font, text=time_display)
#    date = label.Label(font, text=date_display)
    Menu = label.Label(font, text=Menu_display)
#    (_, _, width, _) = Line1.bounding_box
#    clock.x = oled.width // 2 - width // 2
#    clock.y = 5
#    (_, _, width, _) = date.bounding_box
#    date.x = oled.width // 2 - width // 2
#    date.y = 15
    (_, _, width, _) = Menu.bounding_box
    Menu.x = oled.width // 2 - width // 2
    Menu.y = 27
#
    watch_group = displayio.Group()
#    watch_group.append(Line1)
#    watch_group.append(line2)
#    watch_group.append(Menu_display)
    oled.show(Menu)