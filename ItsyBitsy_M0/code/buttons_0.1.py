"""CircuitPython Essentials Digital In Out example"""
import time
import board
from digitalio import DigitalInOut, Direction, Pull

# LED setup.
led = DigitalInOut(board.D13)
# For QT Py M0. QT Py M0 does not have a D13 LED, so you can connect an external LED instead.
# led = DigitalInOut(board.SCK)
led.direction = Direction.OUTPUT

# For Gemma M0, Trinket M0, Metro M0 Express, ItsyBitsy M0 Express, Itsy M4 Express, QT Py M0
switch_midle = DigitalInOut(board.D2)
switch_up = DigitalInOut(board.D9)
switch_dwn = DigitalInOut(board.D7)
# switch = DigitalInOut(board.D5)  # For Feather M0 Express, Feather M4 Express
# switch = DigitalInOut(board.D7)  # For Circuit Playground Express
switch_midle.direction = Direction.INPUT
switch_midle.pull = Pull.UP
switch_up.direction = Direction.INPUT
switch_up.pull = Pull.UP
switch_dwn.direction = Direction.INPUT
switch_dwn.pull = Pull.UP

while True:
    # We could also do "led.value = not switch.value"!
    # midle boton on pin 2
    if switch_midle.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        print("midle boton presed")
    # up boton on pin 9
    if switch_up.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        time.sleep(0.1)
        print("upper boton presed")
    # down bo on pin 7
    if switch_dwn.value:
        led.value = False
    else:
        led.value = False
        time.sleep(1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        time.sleep(0.1)
        led.value = False
        time.sleep(0.1)
        led.value = True
        print("down boton presed")

    time.sleep(0.01)  # debounce delay