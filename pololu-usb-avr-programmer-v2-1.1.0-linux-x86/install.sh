#!/usr/bin/env sh
set -ue
cd "$(dirname "$0")"
D=/usr/local/share/pololu-usb-avr-programmer-v2
rm -vrf $D
mkdir -v $D
cp pavr2cmd pavr2gui *.ttf LICENSE.html $D
ln -vsf $D/pavr2cmd /usr/local/bin/
ln -vsf $D/pavr2gui /usr/local/bin/
cp -v 99-pololu.rules /etc/udev/rules.d/
