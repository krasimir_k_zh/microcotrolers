Pololu USB AVR Programmer v2 software 1.1.0 for linux-x86

To install this software, we recommend starting a terminal, navigating to this
directory using the "cd" command, and then running:

  sudo ./install.sh

The install.sh script will install files and directories to the following
locations on your system:

  /usr/local/bin/pavr2cmd
  /usr/local/bin/pavr2gui
  /usr/local/share/pololu-usb-avr-programmer-v2/
  /etc/udev/rules.d/99-pololu.rules

For more information, see the Pololu USB AVR Programmer v2 User's Guide:

  https://www.pololu.com/docs/0J67
